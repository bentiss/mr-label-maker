Mr Label Maker
==============

Script for automatic labeling of issues and merge requests for projects
hosted on gitlab.freedesktop.org.

Dependencies:

- python3
- pip3

Run instructions:

.. code-block:: sh

  # install from a local checkout
  $ pip3 install --user .

  # or install from the git repo directly
  $ pip3 install --user git+http://gitlab.freedesktop.org/freedesktop/mr-label-maker

  $ ~/.local/bin/mr_label_maker --project mesa --merge-requests --issues --token "$TOKEN" --dry-run

Alternatively, this tool can invoked in response to a GitLab Webhook issue,
confidential issue or merge request event. The webhook JSON data must be provided
either as file or in an environment variable:

.. code-block:: sh

  $ ~/.local/bin/mr_label_maker --token "$TOKEN" --webhook-env="PAYLOAD_VAR"
  $ ~/.local/bin/mr_label_maker --token "$TOKEN" --webhook-file=/path/to/payload.json