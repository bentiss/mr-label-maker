#!/usr/bin/env python3

# Copyright © 2023 Red Hat, Inc.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


import mr_label_maker.mr_label_maker as mlm

from unittest.mock import patch, MagicMock
import json
import pytest

@pytest.fixture
def token() -> str:
    return "topsecret"


# Mesa is the only project we have atm, so we patch that
# whole module. This way none of our tests actually go
# through to the project and we can't accidentally trigger
# real stuff happening.
#
# But it *does* mean we can easily test what the CLI does
# to the backend project - by assigning a MagicMock
# to mesa.Mesa.return_value we can then test that mock
# for all the calls that would've otherwise gone into
# the project itself. Which means we can test the CLI
# without having to care about how (or whether) the
# backend itself works.
@patch("mr_label_maker.mr_label_maker.mesa")
class TestCli:
    def test_invalid_project(self, mesa):
        with pytest.raises(SystemExit) as excinfo:
            mlm.main(['--project', 'foobar'])
        # argparse calls sys.exit(2) for
        # invalid arguments. That's the same as our API_ERROR
        assert excinfo.value.code == mlm.ExitCode.API_ERROR

    def test_missing_token(self, mesa):
        exit_code = mlm.main(['--project', 'mesa'])
        assert exit_code == mlm.ExitCode.USER_ERROR

    @pytest.mark.parametrize("dry_run", (True, False))
    @pytest.mark.parametrize("label", (True, False))
    @pytest.mark.parametrize("state", [None, 'opened', 'closed', 'merged', 'all'])
    @pytest.mark.parametrize("ignore_history", (True, False))
    def test_pass_args(self, mesa, token, dry_run, label, state, ignore_history):
        proj = MagicMock()
        mesa.Mesa.return_value = proj

        # Fixed args because otherwise we don't get anywhere
        args = [
            '--project', 'mesa',
            '--token', token,
        ]
        if dry_run:
            args.append('--dry-run')
        if label:
            args.extend(['--label', 'foolabel'])
        if state:
            args.extend(['--state', state])
        if ignore_history:
            args.append('--ignore-label-history')

        exit_code = mlm.main(args)
        assert exit_code == mlm.ExitCode.SUCCESS

        # Not let's make sure we passed everything through as expected
        proj.set_token.assert_called_with(token)
        proj.set_dry_run.assert_called_with(dry_run)
        proj.set_label.assert_called_with(['foolabel'] if label else [])
        proj.set_state.assert_called_with(state or 'opened')
        proj.set_ignore_label_history.assert_called_with(ignore_history)

        proj.connect.assert_called()

    @pytest.mark.parametrize("arg_name", ["--issues", "-i"])
    @pytest.mark.parametrize("arg", ["skip", "specific", "no-arg"])
    def test_pass_issues(self, mesa, token, arg_name, arg):
        proj = MagicMock()
        mesa.Mesa.return_value = proj

        # Fixed args because otherwise we don't get anywhere
        args = [
            '--project', 'mesa',
            '--token', token,
        ]

        if arg == "skip":
            pass
        elif arg == "specific":
            args += [arg_name, "123"]
        elif arg == "no-arg":
            args += [arg_name]

        exit_code = mlm.main(args)
        assert exit_code == mlm.ExitCode.SUCCESS

        proj.connect.assert_called()
        if arg == "skip":
            proj.process_issues.assert_not_called()
        elif arg == "specific":
            proj.process_issues.assert_called_with(123)
        elif arg == "no-arg":
            proj.process_issues.assert_called_with(-1)

    @pytest.mark.parametrize("arg_name", ["--merge-requests", "-m", "--mrs"])
    @pytest.mark.parametrize("arg", ["skip", "specific", "no-arg"])
    def test_pass_merge_request(self, mesa, token, arg_name, arg):
        proj = MagicMock()
        mesa.Mesa.return_value = proj

        # Fixed args because otherwise we don't get anywhere
        args = [
            '--project', 'mesa',
            '--token', token,
        ]

        if arg == "skip":
            pass
        elif arg == "specific":
            args += [arg_name, "123"]
        elif arg == "no-arg":
            args += [arg_name]

        exit_code = mlm.main(args)
        assert exit_code == mlm.ExitCode.SUCCESS

        proj.connect.assert_called()
        if arg == "skip":
            proj.process_mrs.assert_not_called()
        elif arg == "specific":
            proj.process_mrs.assert_called_with(123)
        elif arg == "no-arg":
            proj.process_mrs.assert_called_with(-1)


    @pytest.mark.parametrize("event_type", ["issue", "confidential_issue", "merge_request", "other"])
    @pytest.mark.parametrize("action", ["open", "close", "update"])
    @pytest.mark.parametrize("payload_from", ["file", "env"])
    def test_webhook_event(self, mesa, token, monkeypatch, tmp_path, capsys,
                           event_type, payload_from, action):
        proj = MagicMock()
        mesa.Mesa.return_value = proj

        hook_data = json.dumps({
            "event_type": event_type,
            "object_kind": event_type,
            "project": {
                "id": 1,
                "name": "Meesa a project name",
                "path_with_namespace": "mesa/mesa"
            },
            "object_attributes": {
                "iid": 23,
                "action": action,
            }
        })

        # Fixed args because otherwise we don't get anywhere
        args = [
            '--project', 'mesa',
            '--token', token,
        ]

        if payload_from == "env":
            monkeypatch.setenv('WEBHOOK_PAYLOAD', hook_data)
            args += ['--webhook-env', 'WEBHOOK_PAYLOAD']
        if payload_from == "file":
            datafile = tmp_path / "payload.data"
            with open(datafile, "w") as fd:
                fd.write(hook_data)
            args += ["--webhook-file", str(datafile)]

        exit_code = mlm.main(args)

        allowed_actions = {
            "issue": ["open"],
            "confidential_issue": ["open"],
            "merge_request": ["open", "update"],
            "other": [],
        }
        if event_type == "other":
            assert exit_code == mlm.ExitCode.USER_ERROR
            assert "Unsupported webhook event" in capsys.readouterr().out
            proj.connect.assert_not_called()
            proj.process_issues.assert_not_called()
            proj.process_mrs.assert_not_called()
        elif action not in allowed_actions[event_type]:
            assert exit_code == mlm.ExitCode.USER_ERROR
            assert "Unsupported action" in capsys.readouterr().out
            proj.connect.assert_not_called()
            proj.process_issues.assert_not_called()
            proj.process_mrs.assert_not_called()
        else:
            assert exit_code == mlm.ExitCode.SUCCESS
            proj.connect.assert_called()
            if event_type in ["issue", "confidential_issue"]:
                proj.process_issues.assert_called_with(23)
                proj.process_mrs.assert_not_called()
            if event_type in ["merge_request"]:
                proj.process_issues.assert_not_called()
                proj.process_mrs.assert_called_with(23)

    @pytest.mark.parametrize("payload_from", ["file", "env"])
    def test_webhook_bad_payload(self, mesa, token, monkeypatch, tmp_path, capsys,
                                 payload_from):
        proj = MagicMock()
        mesa.Mesa.return_value = proj

        # Missing path_with_namespace
        hook_data = json.dumps({
            "event_type": "issue",
            "object_kind": "issue",
            "project": {
                "id": 1,
                "name": "Meesa a project name"
            },
            "object_attributes": {
                "iid": 23,
                "action": "open"
            }
        })

        # Fixed args because otherwise we don't get anywhere
        args = [
            '--project', 'mesa',
            '--token', token,
        ]

        if payload_from == "env":
            monkeypatch.setenv('WEBHOOK_PAYLOAD', hook_data)
            args += ['--webhook-env', 'WEBHOOK_PAYLOAD']
        if payload_from == "file":
            datafile = tmp_path / "payload.data"
            with open(datafile, "w") as fd:
                fd.write(hook_data)
            args += ["--webhook-file", str(datafile)]

        exit_code = mlm.main(args)
        assert exit_code == mlm.ExitCode.BUG
        proj.connect.assert_not_called()
        proj.process_issues.assert_not_called()
        proj.process_mrs.assert_not_called()
        capture = capsys.readouterr()
        assert "Failed to parse payload" in capture.out
        assert "Missing field" in capture.out